#!/bin/sh

cd "$4"
npm install

echo "Linking $5..."
ln -sf "$3" "$4"
