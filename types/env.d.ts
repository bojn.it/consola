namespace NodeJS {
  interface ProcessEnv {
    APP_PORT: string | undefined;

    IDENTITA_DATABASE_TYPE: 'mariadb' | 'mssql' | 'mysql' | 'postgres' | undefined;
    IDENTITA_DATABASE_HOST: string | undefined;
    IDENTITA_DATABASE_PORT: string | undefined;
    IDENTITA_DATABASE_USERNAME: string | undefined;
    IDENTITA_DATABASE_PASSWORD: string | undefined;
    IDENTITA_DATABASE_NAME: string | undefined;
    IDENTITA_DATABASE_CHARSET: 'UTF8_GENERAL_CI'
      | 'UTF8_UNICODE_CI'
      | 'UTF8MB4_GENERAL_CI'
      | 'UTF8MB4_UNICODE_CI'
      | undefined
    ;

    DOXY_DATABASE_TYPE: 'mariadb' | 'mssql' | 'mysql' | 'postgres' | undefined;
    DOXY_DATABASE_HOST: string | undefined;
    DOXY_DATABASE_PORT: string | undefined;
    DOXY_DATABASE_USERNAME: string | undefined;
    DOXY_DATABASE_PASSWORD: string | undefined;
    DOXY_DATABASE_NAME: string | undefined;
    DOXY_DATABASE_CHARSET: 'UTF8_GENERAL_CI'
      | 'UTF8_UNICODE_CI'
      | 'UTF8MB4_GENERAL_CI'
      | 'UTF8MB4_UNICODE_CI'
      | undefined
    ;
  }
}
