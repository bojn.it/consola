/**
 * Suggest standard `Value`(s) of common `Type`(s) to a programmer.
 *
 * @rel https://github.com/microsoft/TypeScript/issues/29729#issuecomment-505826972
 */
type Suggest<Value extends Type, Type extends number | string = string> =
  | Value
  | (Type & {})
