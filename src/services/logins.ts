
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Login } from '../models/login';

@Injectable()
export class LoginsService {
  constructor(
    @InjectRepository(Login)
    private loginsRepository: Repository<Login>,
  ) {}

  findAll(): Promise<Login[]> {
    return this.loginsRepository.find();
  }

  findOne(id: number): Promise<Login | null> {
    return this.loginsRepository.findOneBy({ id });
  }

  async remove(id: string): Promise<void> {
    await this.loginsRepository.delete(id);
  }
}