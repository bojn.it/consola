import { config as loadDotEnv } from 'dotenv';
import { DataSource } from 'typeorm';
import loadDatabaseConfig from '../../config/doxy/database.config';

loadDotEnv();

const config = loadDatabaseConfig();
const type = config.type;
const host = config.host;
const port = config.port;
const username = config.username;
const password = config.password;
const database = config.database;
const entities = ['dist/app/doxy/**/*.entity.js'];
const entityPrefix = 'doxy_';
const migrations = ['dist/database/doxy/migrations/**/*.js'];
const migrationsTableName = 'typeorm_migrations';

let dataSource: DataSource;

switch (type) {
  case 'mariadb':
    dataSource = new DataSource({
      type,
      host,
      port,
      username,
      password,
      database,
      charset: config.charset,
      entities,
      entityPrefix,
      migrations,
      migrationsTableName,
    });
    break;

  case 'mssql':
    dataSource = new DataSource({
      type,
      host,
      port,
      username,
      password,
      database,
      entities,
      entityPrefix,
      migrations,
      migrationsTableName,
    });
    break;

  case 'mysql':
    dataSource = new DataSource({
      type,
      host,
      port,
      username,
      password,
      database,
      charset: config.charset,
      entities,
      entityPrefix,
      migrations,
      migrationsTableName,
    });
    break;

  case 'postgres':
    dataSource = new DataSource({
      type,
      host,
      port,
      username,
      password,
      database,
      entities,
      entityPrefix,
      migrations,
      migrationsTableName,
    });
    break;

  default:
    throw new Error(`Missing logic for '${type}'.`);
}

export default dataSource;
