import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateTicketsTable1684258239648 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.createTable(new Table({
        name: 'doxy_tickets',
        columns: [
          {
            name: 'id',
            type: 'char(36)',
            isPrimary: true,
          },
          {
            name: 'title',
            type: 'varchar(255)',
          },
          {
            name: 'description',
            type: 'text',
          }
        ],
      }), true);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
    }
}
