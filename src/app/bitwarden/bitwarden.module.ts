import { Module } from '@nestjs/common';
import { BitwardenService } from './bitwarden.service';
import { BitwardenController } from './bitwarden.controller';
import { IdentityController } from './identity/identity.controller';

@Module({
  controllers: [BitwardenController, IdentityController],
  providers: [BitwardenService]
})
export class BitwardenModule {}
