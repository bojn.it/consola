import { Test, TestingModule } from '@nestjs/testing';
import { BitwardenService } from './bitwarden.service';

describe('BitwardenService', () => {
  let service: BitwardenService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BitwardenService],
    }).compile();

    service = module.get<BitwardenService>(BitwardenService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
