import { Test, TestingModule } from '@nestjs/testing';
import { BitwardenController } from './bitwarden.controller';
import { BitwardenService } from './bitwarden.service';

describe('BitwardenController', () => {
  let controller: BitwardenController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BitwardenController],
      providers: [BitwardenService],
    }).compile();

    controller = module.get<BitwardenController>(BitwardenController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
