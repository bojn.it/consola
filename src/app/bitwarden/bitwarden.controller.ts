import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { BitwardenService } from './bitwarden.service';
import { CreateBitwardenDto } from './dto/create-bitwarden.dto';
import { UpdateBitwardenDto } from './dto/update-bitwarden.dto';

@Controller('bitwarden')
export class BitwardenController {
  constructor(private readonly bitwardenService: BitwardenService) {}

  @Post()
  create(@Body() createBitwardenDto: CreateBitwardenDto) {
    return this.bitwardenService.create(createBitwardenDto);
  }

  @Get()
  findAll() {
    return this.bitwardenService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bitwardenService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBitwardenDto: UpdateBitwardenDto) {
    return this.bitwardenService.update(+id, updateBitwardenDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.bitwardenService.remove(+id);
  }
}
