import { PartialType } from '@nestjs/mapped-types';
import { CreateBitwardenDto } from './create-bitwarden.dto';

export class UpdateBitwardenDto extends PartialType(CreateBitwardenDto) {}
