import { Controller, Post } from '@nestjs/common';

@Controller('bitwarden/identity')
export class IdentityController {
    @Post('accounts/prelogin')
    prelogin() {}

    @Post('connect/token')
    connectByToken() {}
}
