import { Injectable } from '@nestjs/common';
import { CreateBitwardenDto } from './dto/create-bitwarden.dto';
import { UpdateBitwardenDto } from './dto/update-bitwarden.dto';

@Injectable()
export class BitwardenService {
  create(createBitwardenDto: CreateBitwardenDto) {
    return 'This action adds a new bitwarden';
  }

  findAll() {
    return `This action returns all bitwarden`;
  }

  findOne(id: number) {
    return `This action returns a #${id} bitwarden`;
  }

  update(id: number, updateBitwardenDto: UpdateBitwardenDto) {
    return `This action updates a #${id} bitwarden`;
  }

  remove(id: number) {
    return `This action removes a #${id} bitwarden`;
  }
}
