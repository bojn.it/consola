import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Joi from 'joi';
import { SupportedTypes } from '../../config/doxy/database.config';
import databaseConfig from '../../config/doxy/database.config';
import { TicketsModule } from './tickets/tickets.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],
      expandVariables: true,
      cache: true,
      validationSchema: Joi.object({
        DOXY_DATABASE_TYPE: Joi.valid('mariadb', 'mssql', 'mysql', 'postgres'),
        DOXY_DATABASE_HOST: Joi.string().hostname(),
        DOXY_DATABASE_PORT: Joi.number().min(1024).max(49151),
        DOXY_DATABASE_USERNAME: Joi.string() ,
        DOXY_DATABASE_PASSWORD: Joi.string().allow(''),
        DOXY_DATABASE_DATABASE: Joi.string(),
        DOXY_DATABASE_CHARSET: Joi.valid(
          'UTF8_GENERAL_CI',
          'UTF8_UNICODE_CI',
          'UTF8MB4_GENERAL_CI',
          'UTF8MB4_UNICODE_CI',
        ),
      }),
    }),

    TypeOrmModule.forRootAsync({
      name: 'doxy',
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        type: config.get<SupportedTypes>('doxy.database.type')!,
        host: config.get<string>('doxy.database.host')!,
        port: config.get<number>('doxy.database.port')!,
        username: config.get<string>('doxy.database.username')!,
        password: config.get<string>('doxy.database.password')!,
        database: config.get<string>('doxy.database.database')!,
        charset: config.get<string>('doxy.database.charset'),
        autoLoadEntities: true,
        entityPrefix: 'doxy_',
      }),
      inject: [ConfigService],
    }),

    TicketsModule,
  ],
})
export class DoxyModule {}
