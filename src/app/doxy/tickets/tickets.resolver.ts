import { Resolver, Query, Mutation, Args, Int, ID } from '@nestjs/graphql';
import { TicketsService } from './tickets.service';
import { Ticket } from './entities/ticket.entity';
import { CreateTicketInput } from './dto/create-ticket.input';
import { UpdateTicketInput } from './dto/update-ticket.input';

@Resolver(of => Ticket)
export class TicketsResolver {
  constructor(private readonly ticketsService: TicketsService) {}

  @Mutation(returns => Ticket)
  async createTicket(@Args('input') input: CreateTicketInput): Promise<Ticket> {
    return this.ticketsService.create(input);
  } 

  @Query(returns => [Ticket], { name: 'tickets' })
  findAll() {
    return this.ticketsService.findAll();
  }

  @Query(returns => Ticket, { nullable: true })
  async ticket(@Args('id', { type: () => ID }) id: string): Promise<Ticket | null> {
    return this.ticketsService.findOne(id);
  }

  @Mutation(returns => Ticket)
  updateTicket(@Args('updateTicketInput') updateTicketInput: UpdateTicketInput) {
    return this.ticketsService.update(updateTicketInput.id, updateTicketInput);
  }

  @Mutation(returns => Ticket)
  removeTicket(@Args('id', { type: () => Int }) id: string) {
    return this.ticketsService.remove(id);
  }
}
