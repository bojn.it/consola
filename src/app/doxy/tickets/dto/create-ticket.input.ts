import { InputType, OmitType } from '@nestjs/graphql';
import { Ticket } from '../entities/ticket.entity';

@InputType()
export class CreateTicketInput extends OmitType(Ticket, ['id'] as const, InputType) {}
