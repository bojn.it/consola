import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTicketInput } from './dto/create-ticket.input';
import { UpdateTicketInput } from './dto/update-ticket.input';
import { Ticket } from './entities/ticket.entity';
import { TicketRepository } from './entities/ticket.repository';

@Injectable()
export class TicketsService {
  constructor(
    @InjectRepository(Ticket, 'doxy') private readonly ticketsRepository: TicketRepository,
  ) {}

  async create(input: CreateTicketInput): Promise<Ticket> {
    return this.ticketsRepository.save(input);
  }

  findAll() {
    return this.ticketsRepository.find();
  }

  async findOne(id: string): Promise<Ticket | null> {
    return this.ticketsRepository.findOneBy({id});
  }

  update(id: string, updateTicketInput: UpdateTicketInput) {
    return `This action updates a #${id} user`;
  }

  remove(id: string) {
    return `This action removes a #${id} user`;
  }
}
