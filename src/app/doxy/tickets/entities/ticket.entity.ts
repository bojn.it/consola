import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tickets')
@ObjectType()
export class Ticket {
  @PrimaryGeneratedColumn('uuid') 
  @Field(type => ID)
  readonly id: string;

  @Column({ length: 255 })
  @Field()
  title: string;

  @Column()
  @Field()
  description: string;
}
