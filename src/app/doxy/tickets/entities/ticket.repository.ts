import { Repository } from 'typeorm';
import { Ticket } from './ticket.entity';

export class TicketRepository extends Repository<Ticket> {}
