import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Joi from 'joi';
import { SupportedTypes } from '../../config/identita/database.config';
import databaseConfig from '../../config/identita/database.config';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],
      expandVariables: true,
      cache: true,
      validationSchema: Joi.object({
        IDENTITA_DATABASE_TYPE: Joi.valid('mariadb', 'mssql', 'mysql', 'postgres'),
        IDENTITA_DATABASE_HOST: Joi.string().hostname(),
        IDENTITA_DATABASE_PORT: Joi.number().min(1024).max(49151),
        IDENTITA_DATABASE_USERNAME: Joi.string() ,
        IDENTITA_DATABASE_PASSWORD: Joi.string().allow(''),
        IDENTITA_DATABASE_DATABASE: Joi.string(),
        IDENTITA_DATABASE_CHARSET: Joi.valid(
          'UTF8_GENERAL_CI',
          'UTF8_UNICODE_CI',
          'UTF8MB4_GENERAL_CI',
          'UTF8MB4_UNICODE_CI',
        ),
      }),
    }),

    TypeOrmModule.forRootAsync({
      name: 'identita',
      imports: [ConfigModule],
      useFactory: (config: ConfigService) => ({
        type: config.get<SupportedTypes>('identita.database.type')!,
        host: config.get<string>('identita.database.host')!,
        port: config.get<number>('identita.database.port')!,
        username: config.get<string>('identita.database.username')!,
        password: config.get<string>('identita.database.password')!,
        database: config.get<string>('identita.database.database')!,
        charset: config.get<string>('identita.database.charset'),
        autoLoadEntities: true,
        entityPrefix: 'identita_',
      }),
      inject: [ConfigService],
    }),
  ],
})
export class IdentitaModule {}
