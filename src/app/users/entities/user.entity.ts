import { Field, ObjectType } from '@nestjs/graphql';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users', {database: 'doxy'})
@ObjectType()
export class User {
  @PrimaryGeneratedColumn('uuid')
  @Field()
  readonly id: string;

  @Column({ unique: true, length: 30 })
  @Field()
  username: string;

  @Column({ length: 30 })
  @Field()
  password: string;

  @Column({ default: true })
  @Field({ defaultValue: true })
  isActive: boolean;

  @Column({ nullable: true })
  @Field({ nullable: true })
  comment?: string;
}

