import { ApolloDriver } from '@nestjs/apollo';
import { ApolloDriverConfig } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { ValidationPipe } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { GraphQLModule } from '@nestjs/graphql';
import * as Joi from 'joi';
import appConfig from '../config/app.config';
import { DoxyModule } from './doxy/doxy.module';
import { IdentitaModule } from './identita/identita.module';



@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
      cache: true,
      expandVariables: true,
      validationSchema: Joi.object({
        APP_PORT: Joi.number().min(1024).max(49151),
      }),
    }),



    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      driver: ApolloDriver,
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        autoSchemaFile: true,
        sortSchema: true,
        playground: true,
      }),
    }),

    //IdentitaModule,
    DoxyModule,
  ],
})
export class AppModule {
  static async bootstrap(): Promise<void> {
    const app = await NestFactory.create(AppModule);
    const config = app.get(ConfigService);
    app.useGlobalPipes(new ValidationPipe());
    await app.listen(config.get<number>('app.port')!);
  }
}
