import { registerAs } from '@nestjs/config';

export type SupportedTypes = 'mariadb' | 'mssql' | 'mysql' | 'postgres';

export default registerAs('doxy.database', () => {
  const type = process.env.DOXY_DATABASE_TYPE || 'mysql';

  switch (type) {
    case 'mariadb':
      return {
        type: 'mariadb' as const,
        host: process.env.DOXY_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.DOXY_DATABASE_PORT || '3306'),
        username: process.env.DOXY_DATABASE_USERNAME || 'doxy',
        password: process.env.DOXY_DATABASE_PASSWORD || '',
        database: process.env.DOXY_DATABASE_NAME || 'doxy',
        charset: process.env.DOXY_DATABASE_CHARSET || 'UTF8MB4_UNICODE_CI',
      };

    case 'mssql':
      return {
        type: 'mssql' as const,
        host: process.env.DOXY_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.DOXY_DATABASE_PORT || '1433'),
        username: process.env.DOXY_DATABASE_USERNAME || 'doxy',
        password: process.env.DOXY_DATABASE_PASSWORD || '',
        database: process.env.DOXY_DATABASE_NAME || 'doxy',
      };

    case 'mysql':
      return {
        type: 'mysql' as const,
        host: process.env.DOXY_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.DOXY_DATABASE_PORT || '3306'),
        username: process.env.DOXY_DATABASE_USERNAME || 'doxy',
        password: process.env.DOXY_DATABASE_PASSWORD || '',
        database: process.env.DOXY_DATABASE_NAME || 'doxy',
        charset: process.env.DOXY_DATABASE_CHARSET || 'UTF8MB4_UNICODE_CI',
      };

    case 'postgres':
      return {
        type: 'postgres' as const,
        host: process.env.DOXY_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.DOXY_DATABASE_PORT || '5432'),
        username: process.env.DOXY_DATABASE_USERNAME || 'doxy',
        password: process.env.DOXY_DATABASE_PASSWORD || '',
        database: process.env.DOXY_DATABASE_NAME || 'doxy',
      };

    default:
      throw new Error(`Missing logic for '${type}'.`);
  }
});
