import { registerAs } from '@nestjs/config';

export type SupportedTypes = 'mariadb' | 'mssql' | 'mysql' | 'postgres';

export default registerAs('identita.database', () => {
  const type = process.env.IDENTITA_DATABASE_TYPE || 'mysql';

  switch (type) {
    case 'mariadb':
      return {
        type: 'mariadb' as const,
        host: process.env.IDENTITA_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.IDENTITA_DATABASE_PORT || '3306'),
        username: process.env.IDENTITA_DATABASE_USERNAME || 'identita',
        password: process.env.IDENTITA_DATABASE_PASSWORD || '',
        database: process.env.IDENTITA_DATABASE_NAME || 'identita',
        charset: process.env.IDENTITA_DATABASE_CHARSET || 'UTF8MB4_UNICODE_CI',
      };

    case 'mssql':
      return {
        type: 'mssql' as const,
        host: process.env.IDENTITA_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.IDENTITA_DATABASE_PORT || '1433'),
        username: process.env.IDENTITA_DATABASE_USERNAME || 'identita',
        password: process.env.IDENTITA_DATABASE_PASSWORD || '',
        database: process.env.IDENTITA_DATABASE_NAME || 'identita',
      };

    case 'mysql':
      return {
        type: 'mysql' as const,
        host: process.env.IDENTITA_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.IDENTITA_DATABASE_PORT || '3306'),
        username: process.env.IDENTITA_DATABASE_USERNAME || 'identita',
        password: process.env.IDENTITA_DATABASE_PASSWORD || '',
        database: process.env.IDENTITA_DATABASE_NAME || 'identita',
        charset: process.env.IDENTITA_DATABASE_CHARSET || 'UTF8MB4_UNICODE_CI',
      };

    case 'postgres':
      return {
        type: 'postgres' as const,
        host: process.env.IDENTITA_DATABASE_HOST || 'localhost',
        port: parseInt(process.env.IDENTITA_DATABASE_PORT || '5432'),
        username: process.env.IDENTITA_DATABASE_USERNAME || 'identita',
        password: process.env.IDENTITA_DATABASE_PASSWORD || '',
        database: process.env.IDENTITA_DATABASE_NAME || 'identita',
      };

    default:
      throw new Error(`Missing logic for '${type}'.`);
  }
});
